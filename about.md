---
layout: page
title: About
permalink: /about/
feature-img: "img/sample_feature_img_2.png"
---
<h3> INTRO </h3>
<Table width="auto" border="0">
<tr>
  <td colspan="3"> Hi! I am Manu Chandel</td>
</tr>
<tr>
  <td>Born</td>
  <td colspan="2" >25th Nov 1994</td>
</tr>
<tr>
  <td>Home Town</td>
  <td colspan="2" >Mumbai </td>
</tr>
</Table>
<h3>Education</h3>
<Table width="auto" border="0">
<tr>
  <td>Current Status</td>
  <td colspan="2" >Student</td>
</tr>
<tr>
  <td>Stream</td>
  <td colspan="2" >Computer Science And Engineering </td>
</tr>
<tr>
  <td>Institute</td>
  <td colspan="2" >IIT Roorkee</td>
</tr>
</Table>
<h3>Technical Likes</h3>
<Table width="auto" border="0">
<tr>
  <td colspan="3">I like competitive coding and development</td>
</tr>
<tr>
  <td colspan="3">My area of interest involves data structures and algorithms, operating systems and machine learning</td>
</tr>
</Table>
<h3> Projects</h3>
<Table width="auto" border="0">
<tr>
  <td colspan="3">Centralized Hospital Database Management System on php using phpmyadmin database and Apache.</td>
</tr>
<tr>
  <td colspan="3">A complete automated car parking system on Xilinx platform.</td>
</tr>
<tr>
  <td colspan="3">A small gaming application for windows phone in an event "code.fun.do" organized by Microsoft.</td>
</tr>
<tr>
  <td colspan="3">A full working SIC assembler written in C.</td>
</tr>
<tr>
  <td colspan="3">A math-client server using libpcap library in C.</td>
</tr>
</Table>
